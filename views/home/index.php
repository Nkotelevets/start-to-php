<?php include ROOT . '/views/layouts/header.php'; ?>
<div class="container-links">
  <a class="general-links" href="product">  product list</a>
  <a class="general-links" href="new"> new product</a>
  <a class="general-links general-links-test" href="examples"> Nikolay traning work with OOP PHP</a>
</div>

<div class="general-product-list">
  <?php foreach ($productsList as $product): ?>
    <a href="#" class="item">
      <img src="/template/images/noimage.png" alt="default image">
      <p class="name">Title : <?php echo $product['name']; ?></p>
      <p class="price">Price : <?php echo $product['price']; ?></p>
    </a>
  <?php endforeach; ?>

</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>
