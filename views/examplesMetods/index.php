<?php include ROOT . '/views/layouts/header.php'; ?>
<?php include ROOT . '/views/layouts/goHomeButton.php'; ?>

<div class="container-links">
  <ul>
    <li>
      <a href="/construct">
        constructors and destructors
      </a>
    </li>
    <li>
      <a href="/getset">
        method get() and metod set()
      </a>
    </li>
    <li>
      <a href="/clone">
        metod clone()
      </a>
    </li>
    <li>
      <a href="/inheritance">
        inheritance megic ability
      </a>
    </li>
    <li>
      <a href="/abstract">
        abstarct class
      </a>
    </li>
    <li>
      <a href="/interface">
        interface 
      </a>
    </li>
  </ul>
</div>
<?php include ROOT . '/views/layouts/footer.php'; ?>
