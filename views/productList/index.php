<?php include ROOT . '/views/layouts/header.php'; ?>
<?php include ROOT . '/views/layouts/goHomeButton.php'; ?>

<div>
    <div class="container">
        <div class="row">
          <div class="container-links container-links-left">
            <a class="general-links" href="new"> new product</a>
          </div>
            <h4>List products</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Brand</th>
                    <th>Is New</th>
                    <th>Is Reccomended?</th>
                </tr>
                <?php foreach ($productsList as $product): ?>
                    <tr class="table-row">
                        <td><?php echo $product['id']; ?></td>
                        <td><?php echo $product['name']; ?></td>
                        <td><?php echo $product['price']; ?></td>
                        <td><?php echo $product['brand'];?></td>
                        <td><?php echo $product['is_new'] == 1 ? 'Yes' : 'No';  ?></td>
                        <td><?php echo $product['is_recommended'] == 1 ? 'Yes' : 'No';  ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>
