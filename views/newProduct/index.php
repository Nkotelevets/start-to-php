<?php include ROOT . '/views/layouts/header.php'; ?>
<?php include ROOT . '/views/layouts/goHomeButton.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <h4>Add new product</h4>

            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <div class="col-lg-4">
                <div class="login-form">
                    <form action="#" method="post" enctype="multipart/form-data">
                        <p>Product Name</p>
                        <input type="text" name="name" placeholder="" value="">
                        <p>Price, $</p>
                        <input type="text" name="price" placeholder="" value="">
                        <p>Manufacturer</p>
                        <input type="text" name="brand" placeholder="" value="">
                        <p>Detailed description</p>
                        <textarea name="description"></textarea>
                        <p>Stock availability</p>
                        <select name="availability">
                            <option value="1" selected="selected">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <p>New</p>
                        <select name="is_new">
                            <option value="1" selected="selected">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <p>Recommended</p>
                        <select name="is_recommended">
                            <option value="1" selected="selected">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
