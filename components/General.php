<?php

class General
{

  public function actionCreate()
  {
      if (isset($_POST['submit'])) {
          $options['name'] = $_POST['name'];
          $options['price'] = $_POST['price'];
          $options['brand'] = $_POST['brand'];
          $options['availability'] = $_POST['availability'];
          $options['description'] = $_POST['description'];
          $options['is_new'] = $_POST['is_new'];
          $options['is_recommended'] = $_POST['is_recommended'];

          $errors = false;

          if (!isset($options['name']) || empty($options['name'])) {
              $errors[] = 'Fill Missing fields';
          }

          if ($errors == false) {
              $id = Product::createProduct($options);
              header("");
          }
      }
      require_once(ROOT . '/views/newProduct/index.php');
      return true;
  }

}
