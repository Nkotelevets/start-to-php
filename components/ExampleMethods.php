<?php

class ExampleMethods
{
  public $productName;
  public $productPrice;
  public $productCategory;
  public $productQuantity;
  private $percentSale; // this property will be work only in this class

  public static $productHeight; // example static property
  public static $productWidth; // example static property
  public static $productWeight; // example static property

  public function  __construct($productName, $productPrice, $productCategory, $productQuantity)
  {
    $this->productName = $productName;
    $this->productPrice = $productPrice;
    $this->productCategory = $productCategory;
    $this->productQuantity = $productQuantity;
  }

  public function  __clone()
  {
    $this->identificatorClone = '#thisIsClone';
  }

  public function  __get($name)
  {
    // echo "You get {$name}";
  }

  public function  __set($name, $val)
  {
    $this->$name = $val;
    // echo "You set {$name} with value $val";

  }

  public function showInfoAfterMethodConstruct ()
  {
    $detailsAboutProduct = " Name "."{$this->productName}"." Price "."{$this->productPrice}"." Category "."{$this->productCategory}"." Quantity "."{$this->productQuantity}";
    return $detailsAboutProduct;
  }


  function __destruct() {
       var_dump( "Destruct " . __CLASS__ );
   }
}
