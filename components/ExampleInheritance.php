<?php

class ExampleInheritance extends ExampleMethods
{
  public $info;
  public $instruction;

  public function  __construct($productName, $productPrice, $productCategory, $productQuantity, $info, $instruction)
  {
    parent::__construct($productName, $productPrice, $productCategory, $productQuantity);
    $this->info = $info;
    $this->instruction = $instruction;
  }

  public function showInfoAfterMethodConstruct()
  {
    $detailsAboutProduct = parent::showInfoAfterMethodConstruct();
    $detailsAboutProduct .= " Info "."{$this->info}"." Instruction "."{$this->instruction}";
    return $detailsAboutProduct;
  }
}
