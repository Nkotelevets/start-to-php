<?php

class ExampleInheritanceController
{
    public function actionInheritance()
    {
        $newProductWithAdd = new ExampleInheritance ("Asus", "3000$", "LapTop", "1", "no info", "yes");
        $infoAboutProduct = $newProductWithAdd->showInfoAfterMethodConstruct();
        require_once(ROOT . '/views/examplesMetods/inheritance.php');
        return true;
    }

}
