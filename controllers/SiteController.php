<?php

class SiteController  extends General
{

    public function actionIndex()
    {
        $productsList = Product::getProductsList();
        require_once(ROOT . '/views/home/index.php');
        return true;
    }
}
