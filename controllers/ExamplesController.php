<?php

class ExamplesController
{
    public function actionIndex()
    {
        require_once(ROOT . '/views/examplesMetods/index.php');
        return true;
    }

    public function actionConstruct()
    {
        ExampleMethods::$productHeight = '100';  // set static properties value
        ExampleMethods::$productWidth = '50';  // set static properties value
        ExampleMethods::$productWeight = '20';  // set static properties value

        $newProduct = new ExampleMethods('Macbook pro', '1000$', "Laptop", '100');
        $publicMetodShowInfo = $newProduct->showInfoAfterMethodConstruct();

        require_once(ROOT . '/views/examplesMetods/construct.php');
        return true;
    }

    public function actionClone()
    {
      $newProduct = new ExampleMethods('Iphone Pro Max 256GB', '2000$', "Phone", '10');
      $publicMetodShowInfo = $newProduct->showInfoAfterMethodConstruct();
      $newProductClone = clone $newProduct;
      require_once(ROOT . '/views/examplesMetods/clone.php');
      return true;
    }


    public function actionGetSet()
    {
      $newProduct = new ExampleMethods('Iphone Pro Max 256GB', '2000$', "Phone", '10');
      $newProduct->sold = 'Yes'; // set new oarametr using setter
      $gettedParam = $newProduct->sold; // get new oarametr using getter
      require_once(ROOT . '/views/examplesMetods/getSet.php');
      return true;
    }



}
