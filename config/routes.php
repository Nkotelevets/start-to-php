<?php

return array(

    'product' => 'product/index',
    'new' => 'NewProduct/create',
    'examples' => 'Examples/index',
    'construct' => 'Examples/construct',
    'clone' => 'Examples/clone',
    'getset' => 'Examples/getSet',
    'inheritance' => 'ExampleInheritance/inheritance',
    'abstract' => 'Abstract/abstract',
    'interface' => 'InterfaceTemplate/interface',


    'index.php' => 'site/index',
    '' => 'site/index',
);
