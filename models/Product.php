<?php

class Product
{
      public static function createProduct($options) {
          $db = Db::getConnection();
          $sql = 'INSERT INTO product '
                  . '(name, price, brand, availability,'
                  . 'description, is_new, is_recommended)'
                  . 'VALUES '
                  . '(:name, :price, :brand, :availability,'
                  . ':description, :is_new, :is_recommended)';

          $result = $db->prepare($sql);
          $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
          $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
          $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
          $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
          $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
          $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
          $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
          if ($result->execute()) {
              return $db->lastInsertId();
          }
          return 0;
      }

      public static function getProductsList() {
          $db = Db::getConnection();

          $result = $db->query('SELECT id, name, price, is_new, is_recommended, brand FROM product ORDER BY id ASC');
          $productsList = array();
          $i = 0;
          while ($row = $result->fetch()) {
              $productsList[$i]['id'] = $row['id'];
              $productsList[$i]['name'] = $row['name'];
              $productsList[$i]['price'] = $row['price'];
              $productsList[$i]['is_new'] = $row['is_new'];
              $productsList[$i]['is_recommended'] = $row['is_recommended'];
              $productsList[$i]['brand'] = $row['brand'];

              $i++;
          }
          return $productsList;
      }
}
